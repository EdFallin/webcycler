
class TextReverser {
    reverse(text) {
        // turning ?, etc., back into chars
        text = decodeURIComponent(text);

        // must reverse as an array
        let chars = Array.from(text);
        chars.reverse();

        // back to URL-safe text and out
        text = chars.join("");
        text = encodeURIComponent(text);
        return text;
    }
}

module.exports = TextReverser;
