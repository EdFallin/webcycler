let express = require("express");
let router = express.Router();

let TextReverser = require("../public/javascripts/TextReverser.js");
let reverser = new TextReverser();

router.get("/:word", function(request, response, next) {
    let word = request.params.word;
    word = reverser.reverse(word);
    response.send(word);
});

module.exports = router;
