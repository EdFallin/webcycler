/**/

export class IndexController {
    constructor() {
        this.dom = document;
        this.words = [];
    }

    run() {
        this.wire();
    }

    wire() {
        this.bouncer = this.dom.getElementById("Bouncer");
        this.input = this.dom.getElementById("Input");
        this.output = this.dom.getElementById("Output");
        this.bubble = this.dom.getElementById("BubbleStencil");

        this.bouncer.addEventListener("click", (e) => {
            this.startBouncing(e);
        });
    }

    startBouncing(e) {
        // resetting any output text and styling
        this.output.classList.remove("failed");
        this.output.innerText = null;

        // retaining the individual words,
        // skipping empties from double spaces
        let input = this.input.value;
        this.words = input.split(" ")
            .filter(x => (x != ""));

        // getting the first word
        let word = this.words.shift();

        // starting the bouncing
        this.bounce(word);
    }

    async bounce(word) {
        try {
            // turning ?, etc., into URI-safe text
            word = encodeURIComponent(word);

            // asking for the service to reply
            let reply = await fetch("/service/" + word);

            // region fail path

            if (!reply.ok) {
                throw Error("Fetch failed.");
            }

            // endregion fail path

            // getting the text from the reply
            let text = await reply.text();

            // region finished path

            if (text == null) {
                return;
            }

            // endregion finished path

            // region main path / bouncing

            // first, from URL-safe to normal text,
            // then adding the data to the output
            text = decodeURIComponent(text);
            this.addOutput(text);

            // second, the actual bouncing:
            // sending next word by recursion
            if (this.words.length > 0) {
                let word = this.words.shift();
                this.bounce(word);
            }

            // endregion main path / bouncing
        }
        catch {
            this.output.innerText = "Could not fetch data.";
            this.output.classList.add("failed");
        }
    }

    addOutput(text) {
        let bubble = this.bubble.cloneNode(true);
        bubble.innerText = text;

        this.output.appendChild(bubble);
    }
}
